" Vim syntax file for VELMA assembly language

if exists("b:current_syntax")
	finish
endif

syn case ignore

setlocal iskeyword+=.

syn keyword vOperators ADD SUB MUL DIV CMP TST BIT INC DEC CLR MOV EXCH ASHR
syn keyword vOperators LSHR LSHL BIC BIS BIF NOP
syn keyword vExtra .ORG .WORD .BLK
syn keyword vBranch BR BEQ BNE BGE BGT BLT BLE BVC nextgroup=vMemLoc skipwhite
syn keyword vBranch BVS BCC BCS JSR JUMP nextgroup=vMemLoc skipwhite
syn keyword vBranch RTS RTI
syn keyword vTerm HALT

syn keyword vTodo contained FIXME TODO NOTE XXX

syn match vMemLoc "\w\+" 
syn match vComment ";.*$" contains=vTodo
syn match vLabel "^\s*\w*:"
syn match vConstant "#-\?\w\+" 
syn match vRegister "R[0-7]" 
syn match vIndir "[-@+()]"
syn match vAssignState "^\s*\w\+\s*=\s*\w\+" contains=vMemLoc,vAssignOper
syn match vAssignOper "=" contained

hi def link vOperators Statement
hi def link vBranch Statement
hi def link vExtra PreProc
hi def link vTerm Statement
" hi def link vIndir Operator
hi def link vRegister Type
hi def link vComment Comment
hi def link vTodo Todo
hi def link vConstant Constant
hi def link vLabel Identifier
hi def link vMemLoc Identifier
hi def link vAssignOper Statement
